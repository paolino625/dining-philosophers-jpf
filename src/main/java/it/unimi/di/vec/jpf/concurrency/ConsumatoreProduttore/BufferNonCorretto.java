package it.unimi.di.vec.jpf.concurrency.ConsumatoreProduttore;

public class BufferNonCorretto implements Buffer {

  private int valore;
  private boolean pieno = false;

  public int get() {

    int v;
    while (!pieno) {}
    System.out.println("GOT");
    pieno = false;
    v = valore;
    return v;
  }

  public void put(int value) {
    while (pieno) {}
    System.out.println("PUT");
    valore = value;
    pieno = true;
  }
}
