package it.unimi.di.vec.jpf.concurrency.DiningPhilosopher;

public class Main {

  static int nPhilosophers = 5;

  public static void main(String[] args) {
    if (args.length > 0) {
      nPhilosophers = Integer.parseInt(args[0]);
    }

    Fork[] forks = new Fork[nPhilosophers];
    for (int i = 0; i < nPhilosophers; i++) {
      forks[i] = new Fork();
    }
    for (int i = 0; i < nPhilosophers; i++) {
      Philosopher p =
          i != nPhilosophers - 1
              ? new Philosopher(forks[i], forks[(i + 1) % nPhilosophers])
              : new Philosopher(forks[(i + 1) % nPhilosophers], forks[i]);

      p.start();
    }
  }
}
