package it.unimi.di.vec.jpf.concurrency.ConsumatoreProduttore;

public class Main {

  public static void main(String args[]) throws InterruptedException {

    Buffer buffer =
        new BufferNonCorretto(); // Possibile decidere tra BufferCorretto e BufferNonCorretto.

    Consumatore consumatore = new Consumatore(buffer, 1);
    Produttore produttore = new Produttore(buffer, 1);

    Thread t1 = new Thread(produttore);
    Thread t2 = new Thread(consumatore);

    t1.start();
    t2.start();

    t1.join();
    t2.join();
  }
}
