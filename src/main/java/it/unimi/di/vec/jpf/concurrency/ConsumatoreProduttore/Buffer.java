package it.unimi.di.vec.jpf.concurrency.ConsumatoreProduttore;

public interface Buffer {
  public int get();

  public void put(int value);
}
