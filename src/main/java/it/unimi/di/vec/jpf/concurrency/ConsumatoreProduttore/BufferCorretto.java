package it.unimi.di.vec.jpf.concurrency.ConsumatoreProduttore;

public class BufferCorretto implements Buffer {

  private int valore;
  private boolean pieno = false;

  public synchronized int get() {

    int v;
    while (!pieno) {
      try {
        // System.out.println("Sto per entrare wait get.");
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.out.println("GOT");
    pieno = false;
    v = valore;
    notifyAll();
    return v;
  }

  public synchronized void put(int value) {
    while (pieno) {
      try {
        // System.out.println("Sto per entrare wait put.");
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.out.println("PUT");
    pieno = true;
    valore = value;
    notifyAll();
  }
}
