package it.unimi.di.vec.jpf.concurrency.Triangolo;

public class RealTriangle {

  private final int lato1, lato2, lato3;

  public RealTriangle(int lato1, int lato2, int lato3) {

    this.lato1 = lato1;
    this.lato2 = lato2;
    this.lato3 = lato3;
  }

  public Boolean isInvalidTriangle() {

    if ((lato1 + lato2 <= lato3) || (lato2 + lato3 <= lato1) || (lato1 + lato3 <= lato2)) {

      return Boolean.TRUE;

    } else {

      return Boolean.FALSE;
    }
  }

  public void describe() {

    if (lato1 == lato2 && lato2 == lato3) {
      System.out.print("Triangolo equilatero");
    } else if (lato1 == lato2 || lato2 == lato3 || lato1 == lato3) {
      System.out.print("Triangolo isoscele");
    } else {
      System.out.print("Triangolo scaleno");
    }
  }
}
