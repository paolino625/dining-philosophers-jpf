package it.unimi.di.vec.jpf.concurrency.DiningPhilosopher;

class Philosopher extends Thread {

  Fork left;
  Fork right;

  public Philosopher(Fork left, Fork right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public void run() {
    // think
    synchronized (left) {
      synchronized (right) {
        // eat
      }
    }
  }
}
