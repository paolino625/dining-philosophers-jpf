package it.unimi.di.vec.jpf.concurrency.Triangolo;

import java.util.Random;

public class Main {
  public static void main(String[] args) {

    int lato1, lato2, lato3;

    Random random = new Random(4242);
    lato1 = 10;
    lato2 = 10;
    lato3 = random.nextInt(25);

    RealTriangle triangolo = new RealTriangle(lato1, lato2, lato3);

    if (triangolo.isInvalidTriangle()) {

      throw new IllegalArgumentException();
    }
  }
}
