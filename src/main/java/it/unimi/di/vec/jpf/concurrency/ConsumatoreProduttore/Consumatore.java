package it.unimi.di.vec.jpf.concurrency.ConsumatoreProduttore;

public class Consumatore extends Thread {
  private final Buffer buffer;
  private final int number;

  public Consumatore(Buffer b, int numero) {
    buffer = b;
    this.number = numero;
  }

  public void run() {
    int valore = 0;
    for (int i = 0; i < 10; i++) {
      valore = buffer.get();
    }
  }
}
