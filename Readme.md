**INDICE PARZIALE DINING PHILOSOPHERS**

L'indice totale si può trovare nel repository triangles.

Clonare con git clone --recurse-submodules.
Settare JDK11 e su IDEA lanciare comando "Invalidate Caches/Restart".

| Assignment  | Argomento Lezione | Task Svolti |
| ---------- | --------- | ------------------- |
| [Assignment19](https://gitlab.com/paolino625/dining-philosophers-jpf/-/tree/calcagni_paolo_assignment19)  | JPF. | Implementato modello Consumatore/Produttore. Creato package Triangolo per testare con JPF che triangoli creati casualmente siano validi (uno dei 3 lati deciso da un Random).  |